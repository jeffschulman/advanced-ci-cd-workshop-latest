#### Repository for Advanced CI/CD Workshop

* Workshop is broken up into 7 Sections.
    * Setup
    * Execution Order
    * Rules & Variables
    * Reuse
    * Extension
    * Artifacts
    * Auto DevOps

* Pre-built pipelines, ready to be executed to display the results of workshop scenarios, are stored in labeled branches (i.e. CICD->Pipelines -> Branch section-5).
* Some sections have more than 1 pipeline to be executed - these are directly associated to scenarios 
(i.e. Scenario 4.1 in deck correlates to a completed, executable .gitlab-ci.yml example in branch section-4.1)

#### Goals of Workshop:

* **We want to educate on “How can I do this in GitLab?” for advanced scenarios that are common to GitLab users once they become proficient in base stage/job configuration.**


Reference link for templates project:  https://gitlab-core.us.gitlabdemo.cloud/kkwentus1/ci-shared-resources

Authors: @kkwentus1 and @@juliebyrne
